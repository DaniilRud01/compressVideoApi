import express from "express";
import multer from "multer";
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";
import {compressVideo} from "./compress-video.js";

const upload = multer()
const router = express()
const PORT = 3000;

router.use(cors())
router.use(bodyParser.json(), morgan("tiny"))
router.use(bodyParser.urlencoded({ extended: true }))

router.post("/compress",upload.single("video"), async (req, res) => {
    try {
        const compressFile = await compressVideo(req.file.buffer)
        res.send(compressFile)
    } catch (e) {
        res.send({ code: 500, message: "Failed compress video" });
    }
})

router.get("/", (req, res) => {
    res.send("server working").status(200)
})

router.listen(PORT, () => console.log("server is started port:", PORT))