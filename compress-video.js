import stream from "node:stream";
import ffmpeg from "fluent-ffmpeg";

export const compressVideo = async (buffer) => {
    return new Promise((resolve, reject) => {
        const bufferStream = new stream.PassThrough()
        const buffers = []
        let bufRes = []
        bufferStream
            .on('start', () => {
                console.log('Старт сборки буфера')
            })
            .on('data', function (buf) {
                console.log('Данные буфера =>', buf)
                buffers.push(buf)
            })
            .on('end', async function () {
                bufRes = Buffer.concat(buffers)
            }).on("finish",() => {
            console.log("Сборка буфера завершена")
            resolve(bufRes)
        })

        ffmpeg({
            source: stream.Readable.from(buffer)
        }).outputOptions(['-movflags isml+frag_keyframe'])
            .format("mp4").on('start', (commandLine) => {
            console.log('Создан ffMpeg командой =>' + commandLine);
        }).on('error', function(err) {
            console.log('Не удалось обработать видео' + err.message);
            reject(err)
        }).on('end', function() {
            console.log('Процесс завершен успешно');
        }).writeToStream(bufferStream)
    })
};
